from django.urls import path
from . import views
from .views import category_list, account_list


urlpatterns = [
    path('', views.your_receipts, name='home'),
    path('create/', views.create_receipt, name='create_receipt'),
    path('categories/', category_list, name='category_list'),
    path('accounts/', account_list, name='account_list'),
    path('categories/create/', views.create_category, name='create_category'),
    path('accounts/create/', views.create_account, name='create_account'),

]
