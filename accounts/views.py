from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User
from django.contrib import messages


# Create your views here.


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect('home')
    else:
        form = LoginForm()
    return render(request, 'accounts/login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('login')


def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.get('password')
            password_confirmation = form.cleaned_data.get('password_confirmation')

            if password == password_confirmation:
                if not User.objects.filter(username=form.cleaned_data.get('username')).exists():
                    user = User.objects.create_user(
                        username=form.cleaned_data.get('username'),
                        password=password
                    )
                    auth_login(request, user)
                    messages.success(request, 'Signup successful! Welcome!')
                    return redirect('home')
                else:
                    messages.error(request, 'Username is already taken.')
            else:
                form.add_error('password_confirmation', 'The passwords do not match')
    else:
        form = SignupForm()
    return render(request, 'registration/signup.html', {'form': form})


def home_view(request):
    return render(request, 'accounts/home.html')


def profile_view(request):
    return render(request, 'accounts/profile.html')
